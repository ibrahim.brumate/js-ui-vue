import { RouteRecordRaw } from 'vue-router'

const DogRoutes: RouteRecordRaw = {
  path: '/dogs',

  component: () => import('layouts/MainLayout.vue'),

  children: [
    {
      path: '',
      component: () => import('pages/dogs/IndexPage.vue'),
      name: 'DogsIndex'
    }
  ]
}

export default DogRoutes
